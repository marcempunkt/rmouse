use std::env;
use std::process::Command;

fn print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>())
}

fn main() {
    let arguments: Vec<String> = env::args().collect();
    let mouse: &str = &arguments[1];
    let xinput = Command::new(&format!("xinput"))
        .output()
        .expect("ERrror");
    print!("{:?}", &mouse);
    print!("{:?}", &xinput);
    print_type_of(mouse);
    print_type_of(xinput);
}
